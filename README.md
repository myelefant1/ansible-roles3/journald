## Ansible configuration of journald

This role configures journald with persistent storage.

Tested on debian 10, 11 & 12.

## Role parameters

| name                        | value    | optionnal | default value   | description                              |
| ----------------------------|----------|-----------|-----------------|------------------------------------------|
| journald_system_max_use     | int      | yes       | 1024M           | maximum size of the persistent journal   |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/journald.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: journald
        journald_system_max_use: 1024M
```

## Tests

[tests/tests_journald](tests/tests_journald)
